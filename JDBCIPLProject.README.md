# JDBC - IPL Project --->
- Header Files Used: Connection, DriverManager, ResultSet and Statement
- .jar file connected so as to run the Connection statements
- Coded sequence as required.
Entirety of the project has been acheived in one package, which consists of a single class (Main)

## IPL Project: 

    package com.rxyyxn.jdbc;

    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.ResultSet;
    import java.sql.Statement;

    public class Main {
        public static void main(String[] args) throws Exception {
            String databaseName="rayyandb1", user="postgres", password="rayyan";
            Connection connection = connectOnPgSQLsDatabase(databaseName,user,password);

            Thread.sleep(2000);
            calculateNumberOfMatchesAnnual(connection);
            Thread.sleep(2000);
            getNumberOfWinsOfEachOfficialTeam(connection);
            Thread.sleep(2000);
            get2016ExtrasInfoOfEachTeam(connection);
            Thread.sleep(2000);
            get2015TopEconomicalBowlersInfo(connection);
            Thread.sleep(2000);
            connection.close();
    }


    public static void calculateNumberOfMatchesAnnual(Connection connection)
    {
        try {
            String query="SELECT DISTINCT \"Season\", COUNT(\"Season\") " +
                    "FROM matches " +
                    "GROUP BY \"Season\";";
            Statement connectionStatement = connection.createStatement();
            ResultSet annualCountOfMatches = connectionStatement.executeQuery(query);
            System.out.println("\n1. Number of matches played each season: ");
            while(annualCountOfMatches.next())
                System.out.println(annualCountOfMatches.getString(1)+":" + annualCountOfMatches.getInt(2));
            System.out.println();
        }
        catch(Exception e) {
            System.out.println("Exception found:\n" + e);
        }
    }
        private static void getNumberOfWinsOfEachOfficialTeam(Connection connection) {
            try {
                String query="SELECT DISTINCT \"Winner\", COUNT(\"Winner\") FROM matches " +
                        "GROUP BY \"Winner\" " +
                        "ORDER BY COUNT(\"Winner\") DESC;";
                Statement connectionStatement= connection.createStatement();
                ResultSet matchesDeclaredWonToEachOfficialTeam = connectionStatement.executeQuery(query);
                System.out.println("2. Number of matches won in total by each playing team: ");
                while(matchesDeclaredWonToEachOfficialTeam.next())
                    System.out.println(matchesDeclaredWonToEachOfficialTeam.getString(1)+":" + matchesDeclaredWonToEachOfficialTeam.getInt(2));
                connectionStatement.close();
            }
            catch(Exception e) {
                System.out.println("Exception found:\n" + e);
            }
        }
        public static void get2016ExtrasInfoOfEachTeam(Connection connection)
        {
            try {
                String query="SELECT bowling_team AS BowlingTeam, SUM(extra_runs) AS ExtrasConceded " +
                        "FROM deliveries  JOIN matches " +
                        "ON deliveries.match_id=matches.id " +
                        "WHERE \"Season\"=2016 " +
                        "GROUP BY bowling_team;";
                Statement PostgresConnectionStatement= connection.createStatement();
                ResultSet extraRuns = PostgresConnectionStatement.executeQuery(query);
                System.out.println("\n3. Number of extra runs conceded by each team in the 2016:");
                while(extraRuns.next())
                    System.out.println(extraRuns.getString(1)+":"+extraRuns.getInt(2));
                PostgresConnectionStatement.close();
            }
            catch(Exception e) {
                System.out.println("Exception found:\n" + e);
            }
        }
        public static void get2015TopEconomicalBowlersInfo(Connection connection)
        {
            try {
                String myQuery="SELECT bowler,SUM(total_runs)/((COUNT(bowler)/6) +((MOD(COUNT(bowler),6))/6)) AS economy " +
                        "FROM deliveries JOIN matches ON deliveries.match_id = matches.id " +
                        "WHERE matches.\"Season\" =2015 " +
                        "GROUP BY bowler " +
                        "ORDER BY economy LIMIT 10;";
                Statement PostgresConnectionStatement= connection.createStatement();
                ResultSet bestEconomyBowlers=PostgresConnectionStatement.executeQuery(myQuery);
                System.out.println("\n4 List of top economical bowlers: ");
                while(bestEconomyBowlers.next())
                    System.out.println("  "+bestEconomyBowlers.getString("bowler")+":"+bestEconomyBowlers.getInt(2));
                PostgresConnectionStatement.close();
            }
            catch(Exception e) {
                System.out.println(e);
            }
        }

        public static Connection connectOnPgSQLsDatabase(String databaseName, String user, String password) throws InterruptedException {
            Connection pgsqlToJava = null;
            try{
                Class.forName("org.postgresql.Driver");
                pgsqlToJava = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + databaseName,user,password);
                if (pgsqlToJava!= null)
                    System.out.println("Connection to database established.");
                else
                    System.out.println("Connection to database failed!");
            }
            catch (Exception e) {
                System.out.println(e);
            }
            return pgsqlToJava;
        }
    }
## 🚀 About Me
Soon to be full stack developer...


## 🛠 Skills
Python, Java, C++, MySQL, PostGRES, HTML, CSS, Javascript...


## Lessons Learned
So today we have learnt about how to install PostgreSQL; secondly, about how to create and delete users and databses on the SQl; 
thirdly, we have learnt about how to use CSV files into a databse's table; fourthly, we have shown the commands required in order to execute a specific project assigned by the company we are part of on this day.