# What is the Structured Query Language ?
The Structured Query Language is a domain-specific language used in programming, mainly essential in the usage of structured data; which means that it is used for handling data incorporating relations amidst variables and entities.

- PostgreSQL, also known as Postgres, is a free and open-source relational database management system emphasizing extensibility and SQL compliance. It was originally named POSTGRES, referring to its origins as a successor to the Ingres database developed at the University of California, Berkeley.
- Follow the given step by step instructions in order to complete an IPL Project which you may have been assigned. . .

## Install postgreSQL 
Log into your Ubuntu system and update the system software packages using the following:
### $ sudo apt update
Install the latest version of PostgreSQL from default Ubuntu repositories:
### $ sudo apt install postgresql
If the above does not work, use this:
### $ sudo apt install postgresql postgresql-contrib
- If neither works, no need to worry. Try just read the provided suggestion which may have been provided by the Ubuntu terminal. . .

After installed, make sure that the service is started:

    - $ sudo systemctl is-active postgresql
    - $ sudo systemctl is-enabled postgresql
    - $ sudo systemctl status postgresql

Now log into postgreSQL:
### $ sudo -i -u postgres
Now access the Postgres prompt:
### psql 
you are connected default database initially, but you may exit using: \q, and login again using: psql

## CREATION- DELETION OF users

Create a new  user using the following:
### create user newUser with password "newPassword";

 Create another user using the same:

 ### create user newerUser with password "newerPassword";

Creation of any user as a SUPERUSER is done by the following command:
 ### ALTER USER userX WITH SUPERUSER;
 ### alter user newUser with superuser;
Deletion are done with the following . . .
 ### drop user newerUser;
- Well done bois. Moving on to next steps. . . 
## Creation of a new database can be implemented using:

### createdb rayyansdb1

## Delete (that is, DROP) the databases by implementing:

### dropdb rayyansdb2
- Well done boiys. Moving on to CSV file usage. . . 

# Loading of CSV file :
 There are two ways with which we import files into a database's table, viz. (viz. = which are) - - -
  - Using postgres shell
  - using Pgadmin4 

  Use copy command and thus, import the data in your target table.

    - COPY FROM 'location + file_name' DELIMITER ',' CSV HEADER; <table name> – the name of the table you want to import data into. 'location + file_name' – the full path to the file you're importing data from.
    or
    - \copy target_table (column-1, column-2, column-3, ...)
    from '/path/to/local/filename.csv' WITH DELIMITER ',' CSV HEADER;

To import CSV using this PgAdmin Import CSV method, you have to do the following: 
    
    - Click on the Tools tab at the top of your PgAdmin Home Page. 
    - Select the Query Tool in the drop-down menu that appears. 
    - Enter the title of the columns in your CSV file as an SQL Query.
    - Don't forget to check boxes for the primary keys in the third tab.
    - Click save once all title headings and primary keys are set.
- To create tables before importing csv files, you must navigate on the left options, click on your database name, 
    -
- Then right click and create the tables on those databases.
    -
- Then add your required column names (headings)
    -
- These must be done before importing CSV by right clicking on the <tabelname> and getting into Import/Export Data!
    -

In case of trouble, my email address is:  rayyan.22@mountblue.tech

### #Setup complete!
- Well done bois. Now solution of project is listed below. . . . .
- - - - - - - - - 
# SOLVED QUERIES FROM IPLPROJECT (JAVA-BATCH) - - - 
### Number of matches played per year of all the years in IPL.

    SELECT DISTINCT "Season", COUNT("Season") 
    FROM matches 
    GROUP BY "Season";

### Number of matches won of all teams over all the years of IPL.

    SELECT DISTINCT "Winner", COUNT("Winner") FROM matches
    GROUP BY "Winner" 
    ORDER BY COUNT("Winner") DESC ;

### For the year 2016 get the extra runs conceded per team.
    SELECT bowling_team AS BowlingTeam, SUM(extra_runs) AS ExtrasConceded
    FROM deliveries  JOIN matches
    ON deliveries.match_id=matches.id 
    WHERE "Season"=2016 
    GROUP BY bowling_team;

### For the year 2015 get the top economical bowlers.
    SELECT bowler,SUM(total_runs)/((COUNT(bowler)/6) +((MOD(COUNT(bowler),6))/6)) AS economy 
    FROM deliveries JOIN matches ON deliveries.match_id = matches.id 
    WHERE matches."Season" =2015 
    GROUP BY bowler 
    ORDER BY economy LIMIT 10;

## 🚀 About Me
Soon to be full stack developer...


## 🛠 Skills
Python, Java, C++, MySQL, PostGRES, HTML, CSS, Javascript...


## Lessons Learned
So today we have learnt about how to install PostgreSQL; secondly, about how to create and delete users and databses on the SQl; 
thirdly, we have learnt about how to use CSV files into a databse's table; fourthly, we have shown the commands required in order to execute a specific project assigned by the company we are part of on this day.

## 🚀 About Me
Soon to be full stack developer...


## 🛠 Skills
Python, Java, C++, MySQL, PostGRES, HTML, CSS, Javascript...


## Lessons Learned
So today we have learnt about how to install PostgreSQL; secondly, about how to create and delete users and databses on the SQl; 
thirdly, we have learnt about how to use CSV files into a databse's table; fourthly, we have shown the commands required in order to execute a specific project assigned by the company we are part of on this day.