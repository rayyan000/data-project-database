_________________________________________________________________________________________
/*	Number of matches played per year of all the years in IPL.	*/

SELECT DISTINCT "Season", COUNT("Season") 
FROM matches 
GROUP BY "Season";
_________________________________________________________________________________________

/*	Number of matches won of all teams over all the years of IPL.	*/

SELECT DISTINCT "Winner", COUNT("Winner") FROM matches
GROUP BY "Winner" 
ORDER BY COUNT("Winner") DESC ;
_________________________________________________________________________________________

/*	For the year 2016 get the extra runs conceded per team.	*/

SELECT bowling_team AS BowlingTeam, SUM(extra_runs) AS ExtrasConceded
FROM deliveries  JOIN matches
ON deliveries.match_id=matches.id 
WHERE "Season"=2016 
GROUP BY bowling_team;
_________________________________________________________________________________________

/*	For the year 2015 get the top economical bowlers.	*/

SELECT bowler,SUM(total_runs)/((COUNT(bowler)/6) +((MOD(COUNT(bowler),6))/6)) AS economy 
FROM deliveries JOIN matches ON deliveries.match_id = matches.id 
WHERE matches."Season" =2015 
GROUP BY bowler 
ORDER BY economy LIMIT 10;
_________________________________________________________________________________________
